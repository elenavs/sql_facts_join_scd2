-- Создаем таблицу DEMIPT2.XXXX_SALARY_HIST с SCD2 версией таблицы DE.HISTGROUP. Скрипт, выводит таблицу платежей сотрудникам. --
-- MONTH_PAID - суммарно выплачено в месяце, MONTH_REST - осталось выплатить за месяц. --

SELECT * FROM demipt2.suha_salary_hist;

SELECT
t1.payment_dt,
t1.person,
t1.payment,
SUM (payment) OVER (PARTITION BY t1.person, t1.month ORDER BY payment_dt) month_paid,
salary - SUM (payment) OVER (PARTITION BY t1.person, t1.month ORDER BY payment_dt) month_rest
FROM
    (SELECT
    payment_dt,
    person,
    payment,
    EXTRACT (MONTH FROM payment_dt) month
    FROM de.payments) t1
LEFT JOIN demipt2.suha_salary_hist t2
ON t1.person = t2.person AND t1.payment_dt BETWEEN t2.effective_from AND t2.effective_to
ORDER BY t1.person, t1.payment_dt;
